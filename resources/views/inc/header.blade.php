<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <link rel="stylesheet" href="{{ asset('assets/theme/plugins/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/theme/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/theme/plugins/sweetalert2/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/theme/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/theme/plugins/toastr/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/theme/plugins/datatables/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('assets/theme/plugins/bootstrap-switch/css/bootstrap3/bootstrap-switch.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/theme/css/adminlte.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/custom.css') }}">
    <link rel="icon" href="{{  asset('image/siteSettings/' . siteTitle()[1]) }}" type="image/ico">

    <style>
        #cover {
            position: fixed;
            height: 100%;
            width: 100%;
            top: 0;
            left: 0;
            background: #212529;
            z-index: 9999;
            font-size: 65px;
            text-align: center;
            padding-top: 200px;
            color: #fff;
            font-family: inherit;
        }

        .logo_cover {
            margin-top: 200px
        }

    </style>
    @stack('style')
</head>

<body
    class="hold-transition dark-mode sidebar-mini sidebar-collapse layout-fixed layout-navbar-fixed layout-footer-fixed">
    <div class="wrapper">

        <div id="cover"> <span class="logo_cover fas fa-sync fa-spin"></span> loading...</div>
        {{-- <span> <img src="{{ asset('assets/theme/img/loader.jpg') }}"> </span> --}}
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-dark">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i
                            class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{ route('index') }}" class="nav-link {!! \Request::route()->getName() == 'index' ? 'active' : '' !!}">Home</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{ route('category.index') }}" class="nav-link  {!! in_array(\Request::route()->getName(), CategoryRouts()) ? 'active' : '' !!}">Category</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="{!! route('product-index') !!}" class="nav-link {!! in_array(\Request::route()->getName(), ProductRouts()) ? 'active' : '' !!}">Product</a>

                </li>
                 <li class="nav-item d-none d-sm-inline-block">
                    <a href="{!! route('site-settings.index') !!}" class="nav-link {!! \Request::route()->getName() == 'site-settings.index' ? 'active' : '' !!}">Site-Settings</a>

                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item" id='ct7'></li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <div class="top-right">
                    <div class="header-menu">
                        <div class="user-area dropdown float-right">
                            <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">Welcome {{ username() }}</a>
                            <div class="user-menu dropdown-menu">
                                <a class="nav-link" href="{{ route('logout.perform') }}"><img
                                        class="wc mb-1"
                                        style="height: 35px; width: 35px; border-top-left-radius: 50% 50%; border-top-right-radius: 50% 50%; border-bottom-right-radius: 50% 50%; border-bottom-left-radius: 50% 50%;"
                                        src="{{ asset('assets/theme/img/chalta.jpg') }}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Logout</a>
                            </div>
                        </div>
                    </div>
                </div>
            </ul>

        </nav>
