@extends('layout.app')
@section('title', $title)
@section('head', $title)
@push('style')
    <style>
        .hide {
            display: none;
        }

        .show {
            display: block;
        }

        .image-preview__image {
            width: 120px;
            height: 150px;
        }

    </style>
@endpush

@section('main')

    <section class="content">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">{!! $title !!}</h3>
            </div>
            <form action="{{ route('site-settings.save') }}" method="POST" enctype="multipart/form-data" id="saveForm" data-parsley-validate>
                @csrf
                <div class="card-body">
                    {{-- {!! dd($data->)!!} --}}
                    <div class="image-preview {{ isset($data[1]) ? 'show' : 'hide' }} ">
                        <img id="fileList" class="image-preview__image" src="{{  isset($data[1]) ? $data[1] ? asset('image/siteSettings/' . $data[1]) : '' : '' }}">
                    </div>

                    <div class="form-group">
                        <label for="siteSettings">Site-Logo: </label>
                        <input name="site_logo" accept="image/*" type="file" id="fileElem" value="{!! isset($data[1]) ? $data[1] : '' !!}" class="form-control"
                            onchange="preview(event)">
                    </div>
                    <div class="form-group">
                        <label for="siteSettings">Site-Title: </label>
                        <input name="site_title" type="text" placeholder="Enter Title" class="form-control" autocomplete="off"
                            value="{!! isset($data[0]) ? $data[0] : '' !!}">
                    </div>
                    <input name="submit" value="Submit" type="submit" class="btn  btn-primary ">
                </div>
            </form>
        </div>
    </section>
@endsection
