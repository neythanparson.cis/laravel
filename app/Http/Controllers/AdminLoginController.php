<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AdminLoginController extends Controller
{
    public function index()
    {

        return view('login');
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'Required',
            'password' => 'Required'
        ]);
        
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return redirect('index')->with('success','welcome' ." ". username());
        } else {
            return redirect('login')->with('error','Please enter valid login details!');
        }
    }
    
    public function display()
    {
        return view('index');
    }

    public function logout()
    {
        Auth::logout(); 
        return redirect('login');
    }
}
