<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    public $table = 'tbl_product_img';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $fillable = ['i_product_id','v_image','ti_main_image'];
}
