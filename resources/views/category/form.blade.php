@extends('layout.app')
@section('title', $title)
@section('head', $title)
@push('style')
    <style>
        .hide {
            display: none;
        }

        .show {
            display: block;
        }

        .image-preview__image {
            width: 120px;
            height: 150px;
        }

    </style>
@endpush

@section('main')

    <section class="content">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">{!! $title !!}</h3>
            </div>

            <form action="{!! $urlSave !!}" method="POST" enctype="multipart/form-data" id="saveForm"
                data-parsley-validate>
                @csrf
                <div class="card-body">
                    {{-- {!! dd($category->id) !!} --}}
                    <div class="image-preview {{ isset($category->v_image) ? 'show' : 'hide' }} ">
                        <img id="fileList" class="image-preview__image"
                            src="{{ $category->v_image ? asset('image/category/' . $category->v_image) : '' }}">
                    </div>

                    <div class="form-group">
                        <label for="categories">Category Image: </label>
                        <input name="v_image" accept="image/*" type="file" id="fileElem" value="{!! $category->v_image !!}"
                            class="form-control" onchange="preview(event)" {!! \Request::route()->getName() == 'category.categoryAdd' ? 'required' : '' !!}
                            data-parsley-required-message="Please choose Image">
                        <span class="text-danger error-text error_err"></span>
                    </div>
                    <div class="form-group">
                        <label for="categories">Category Name: </label>
                        <input name="v_name" type="text" placeholder="Enter Category" class="form-control"
                            autocomplete="off" value="{!! $category->v_name !!}" required
                            data-parsley-required-message="Please enter category name">
                    </div>
                    <div class="form-group">
                        <label for="categories">Orders:</label>
                        <input name="i_order" type="text" placeholder="Enter Order" class="form-control"
                            autocomplete="off" value="{!! $category->i_order !!}" required
                            data-parsley-required-message="Please enter no. of orders">
                    </div>
                    <div class="form-group">
                        <label for="categories">Status:</label>
                        <select name="ti_status" class="form-control custom-select" id="myselect" required
                            data-parsley-required-message="Please select status">
                            <option value="" selected>Status</option>
                            <option value="1"
                                {{ isset($category->ti_status) && $category->ti_status == 1 ? 'selected' : '' }}>
                                Active
                            </option>
                            <option value="0"
                                {{ isset($category->ti_status) && $category->ti_status == 0 ? 'selected' : '' }}>
                                Deactive
                            </option>
                        </select>
                    </div>
                    <input name="submit" value="Submit" type="submit" class="btn  btn-primary ">
                    {{-- <span name="submit">Submit</span> --}}
                </div>
            </form>
        </div>
    </section>
@endsection
@push('script')
    <script type='text/javascript'>
        jQuery("#saveForm").submit(function(e) {
            e.preventDefault();
            var validate = $('#saveForm').parsley();
            if (validate.isValid()) {
                var data = new FormData($('#saveForm')[0]);
                var url = "{!! $urlSave !!}";
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                setInterval(() => {
                    $("#cover").fadeOut(3000);
                }, 3000);
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    mimeType: "multipart/form-data",
                    dataType: "json",
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        //console.log(response)-> {success: 'Category Add Success', url: 'http://localhost/ecom/category'}
                        if ($.isEmptyObject(response.err)) {
                            printSuccessMsg(response.success);

                            setInterval(function() {
                                window.location.href = response.url;
                            }, 1500);

                        } else {
                            printErrorMsg(response.err);
                        }
                    }

                });
            }
        });
    </script>
    <script></script>
@endpush
