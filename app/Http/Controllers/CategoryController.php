<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use PHPUnit\Framework\Constraint\Count;
 
class CategoryController extends Controller
{
    public function loadTable()
    {
        $query = Category::query();
        $query = $query->select('tbl_categories.*', DB::raw('count(tbl_product_categories.bi_product_id) AS no_product'));
        $query = $query->leftJoin('tbl_product_categories', 'tbl_product_categories.bi_category_id', '=', 'tbl_categories.id');

        if (request('search'))
            $query = $query->where('v_name', 'like', '%' . request('search') . '%');

        if (!empty(request('status')) && request('status') == 'active') {
            $query = $query->where('ti_status', 1);
        } elseif (!empty(request('status')) && request('status') == 'inactive') {
            $query = $query->where('ti_status', 0);
        }
        $query = $query->groupBy('tbl_categories.id');
        $query = $query->orderBy(request('columns.' . request('order.0.column') . '.name'), request('order.0.dir'));
        //request('order.0.column') => '0'
        //request('columns.' . request('order.0.column') . '.name') => 'id'
        //request('columns') =>  0 => array:5 [
        //     "data" => "id"   
        //     "name" => "id" -> column name of table
        //     "searchable" => "true"
        //     "orderable" => "true"
        //     "search" => array:2 [
        //       "value" => null
        //       "regex" => "false"
        //     ]
        //   ]
        //request('order.0.dir') => 'asc'
        $total = count($query->get());
        $list = $query->skip(request('start'))->limit(request('length'))->get();
        //request('start') => 0
        //request('length') => 10 -> according to show entries eg.10-25-50-100
        $data = [];
        foreach ($list as $key => $val) {
            $data[] = [
                'id' =>  $val->id,
                'v_image' => '<img height="100px" width="120px" src="' . asset('image/category/' . $val->v_image) . '">',
                'v_name' => $val->v_name,
                'dt_added_on' => $val->dt_added_on,
                'dt_modified_on' =>  $val->dt_modified_on,
                'no_product' => $val->no_product,
                'i_order' =>  $val->i_order,
                'ti_status' => ($val->ti_status) ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Inactive</span>',
                'action' => '</div><a  class="btn btn-warning" href=' . route('category.categoryEdit', $val->id) . '><i class="fas fa-pencil-alt"></i></a> <button  data-id="' . $val->id . '" class="btn btn-danger delete"><i class="fas fa-trash"></i></button>'
            ];
        }
        // dd($data);
        return response()->json([
            'draw'  => request('draw'),
            'recordsTotal' => $total,
            'recordsFiltered' => $total,
            'data' => $data
        ]);
    }

    public function index()
    {
        $title = 'Category-List';
        return view('category.list', ['title' => $title]);
    }

    public function form($id = 0)
    {
        $title = $id ? 'Edit Category' : 'Add Category';
        $category = Category::find($id);
        $category = $category ? $category : new Category;
        $urlsave = $id ? route('category.categorySave', [$category->id]) : route('category.categorySave', [0]);
        return view('category.form', ['category' => $category, 'urlSave' => $urlsave, 'title' => $title]);
    }

    public function save($id, Request $request)
    {
        $msg = [
            "v_name.required" => 'Category Name is Required!',
            "v_name.unique" => 'Category Name Already Exists!',
            "v_image.required" => 'Please Select Image!',
            "i_order.required" => 'Order is Required!',
            "ti_status.required" => 'Please Select Status!'
        ];

        $validator = validator::make(
            $request->all(),
            [
                'v_name' => 'required',
                'i_order' => 'required|numeric',
                'ti_status' => 'required'
            ],
            $msg
        );

        if (!$id) {
            $validator = validator::make($request->all(), [
                'v_image' => 'required',
                'v_name' => 'required|unique:tbl_categories,v_name',
                'i_order' => 'required|numeric',
                'ti_status' => 'required'
            ], $msg);
        }
        // dd($validator->errors()->all()[0]);
        if ($validator->fails()) {
            return response()->json(['err' => $validator->errors()->all()[0]]);
        }
        $ext = ['jpg', 'jpeg', 'png', 'gif'];
        $category = Category::find($id);
        $category = $category ? $category : new Category;
        // dd($request->v_image);
        if ($request->v_image) {
            if (in_array($request->v_image->extension(), $ext)) {
                $image_name = time() . '_' . $request->categoryname . '.' . $request->v_image->extension();
                $request->v_image->move(public_path('image/category'), $image_name);
                $category->v_image = $image_name;
            } else {
                return response()->json(['err' => "Image must be in jpg/jpeg/png/gif format"]);
            }
        }
        $category->v_name = $request->v_name;
        $category->i_order = $request->i_order;
        $category->ti_status = $request->ti_status;

        if ($category->save()) {
            return response()->json(['success' => 'Category Add Success', 'url' => route('category.index')]);
        }
    }

    public function delete(Request $request)
    {
        $category = Category::find($request->id);
        $category->delete();
        return response()->json(['success' => 'Deleted']);
    }
}
