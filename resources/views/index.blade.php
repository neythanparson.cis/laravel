@extends('layout.app')
@section('title', 'index')
@section('head', 'Dashboard')
@section('main')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3>{!! countCategories() !!}</h3>
                            <p>Total Categories</p>
                            <p>Active Categories<span class="float-right badge bg-warning">{!! countCategoriesActive() !!}</span>
                            </p>
                            <p>Inactive Categories<span class="float-right badge bg-danger">{!! countCategoriesDeactive() !!}</span>
                            </p>
                        </div>
                        <div class="icon">
                            <i class="far fa-list-alt list_icon"></i>
                        </div>
                        <a href="{{ route('category.index') }}" class="small-box-footer">More info <i
                                class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <div class="col-lg-3 col-6">
                    <div class="small-box bg-success">
                        <div class="inner">
                            <h3>{!! countProducts() !!}</h3>
                            <p>Total Products</p>
                            <p> Active Products<span class="float-right badge bg-warning">{!! countProductsActive() !!}</span></p>
                            <p>Inactive Products<span class="float-right badge bg-danger">{!! countProductsDective() !!}</span></p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-shopping-basket cart_icon"></i>
                        </div>
                        <a href="{!! route('product-index') !!}" class="small-box-footer">More info <i
                                class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
             
    </section>
@endsection
