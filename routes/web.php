<?php

use Illuminate\Support\Facades\Route;
use GuzzleHttp\Middleware;
use App\Http\Controllers\AdminLoginController;
use App\Http\Controllers\CategoryController;
use \App\Http\Controllers\ProductController;
use App\Http\Controllers\SiteSettingsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['middleware' => 'guest'], function () {
    Route::get('login', [AdminLoginController::class, 'index'])->name('login.index');
    Route::post('login/submit', [AdminLoginController::class, 'login'])->name('login.submit');
});

Route::group(["middleware" => "prevent-back-history"], function () {
    Route::group(["middleware" => "adminAuth"], function () {
        Route::get('/logout', [AdminLoginController::class, 'logout'])->name('logout.perform');
        Route::get('index', [AdminLoginController::class, 'display'])->name('index');

        // Categories Routes
        Route::get('category', [CategoryController::class, 'index'])->name('category.index'); //category table 
        Route::POST('loadlist', [CategoryController::class, 'loadTable'])->name('category.loadList');
        Route::get('categoryadd', [CategoryController::class, 'form'])->name('category.categoryAdd'); //category add (form)
        Route::get('category/edit/{id}', [CategoryController::class, 'form'])->name('category.categoryEdit'); //category edit(form)
        Route::post('categorysave/{id}', [CategoryController::class, 'save'])->name('category.categorySave'); //category save (add/update)
        Route::post('category/delete', [CategoryController::class, 'delete'])->name('category.categoryDelete');

        // Product Routes
        Route::get('product', [ProductController::class, 'index'])->name('product-index');
        Route::POST('/product-list', [ProductController::class, 'loadList'])->name('product.product-list');
        Route::get('/product-add', [ProductController::class, 'form'])->name('product.prodcut-add');
        Route::get('/product-edit/{id}', [ProductController::class, 'form'])->name('product.prodcut-edit');
        Route::post('/product-save/{id}', [ProductController::class, 'save'])->name('product.prodcut-save');
        Route::post('/product-delete', [ProductController::class, 'delete'])->name('product.prodcut-delete');
        Route::post('/product-img-delete', [ProductController::class, 'imageDelete'])->name('product.image-delete');

        // Site Settings Routes
        Route::get('site-settings', [SiteSettingsController::class, 'index'])->name('site-settings.index');
        Route::POST('site-settings-save', [SiteSettingsController::class, 'save'])->name('site-settings.save');
    });
});
