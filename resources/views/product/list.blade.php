@extends('layout.app')
@section('title', 'Product')
@section('head', 'Products')

@section('main')
    <section class="content">
        <div class="container-fluid">
            <!-- Default box -->

            <div class="card card-outline card-info collapsed-card">
                <div class="card-header">
                    <h3 class="card-title">Search</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-plus"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body" style="display: none;">
                    <form id="search_form" method="POST">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" name="product_search" id="product_search" placeholder="Product Name"
                                        class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" id="min_price" name="min_price" placeholder="Min-price"
                                        class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" id="max_price" name="max_price" placeholder="Max-price"
                                        class="form-control" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" id="min_qty" name="min_qty" placeholder="Min-qty"
                                        class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" id="max_qty" name="max_qty" placeholder="Max-qty"
                                        class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select class="form-control" name="product_status" id="product_status">
                                        <option value="" selected>Status</option>
                                        <option value="active">Active</option>
                                        <option value="inactive">Deactive</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="submit" value="Search" name="search" class="btn btn-info">
                                    <input type="button" value="Reset" name="reset" class="btn btn-danger" id="clear_btn">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
            <div class="card card-outline card-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-12 col-lg-6">
                            <h3 class="font-weight-bold">{!! $title !!}</h3>
                        </div>
                        <div class="col-sm-12 col-lg-6">
                            <a href="{!! route('product.prodcut-add') !!}" type="button" class=" btn btn-primary float-right"><i
                                    class="fa fa-plus" aria-hidden="true">&nbsp;</i> Add New Record!</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-borderless table-hover dtTable" id="productTable">
                        <thead>
                            <tr>
                                <th data-data="id" data-name="id">ID </th>
                                <th data-data="v_image" data-name="v_image" data-orderable=false>Image</th>
                                <th data-data="v_name" data-name="v_name">Name</th>
                                <th data-data="i_product_code" data-name="i_product_code" data-orderable=false>Product Code
                                </th>
                                <th data-data="v_category_name" data-name="v_category_name">Categories Name</th>
                                <th data-data="f_price" data-name="f_price">Price</th>
                                <th data-data="f_sale_price" data-name="f_sale_price">Sale_price</th>
                                <th data-data="i_qty" data-name="i_qty">Qty</th>
                                <th data-data="dt_added_on" data-name="dt_added_on">Added Date</th>
                                <th data-data="dt_modified_on" data-name="dt_modified_on">Modified
                                    Date/Time</th>
                                <th data-data="i_order" data-name="i_order">Order</th>
                                <th data-data="ti_status" data-name="ti_status">Status</th>
                                <th data-data="action" data-name="action" data-orderable=false>Action</th>
                            </tr>
                        </thead>
                        <tbody>


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('script')
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var proTable = applyDtTable('#productTable', "{{ route('product.product-list') }}");
            // console.log(proTable);

            $(document).on("click", ".delete", function(e) {
                e.preventDefault;
                var el = $(this)
                // console.log(el);
                var delete_id = $(this).data('id');
                // alert(delete_id);
                Swal.fire({
                    title: 'Ssly Bruh! U wanna Delete it?',
                    // text: "You won't be able to revert Data!",
                    // icon: 'warning',
                    imageUrl: "{{ asset('assets/theme/img/error.gif') }}",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        var data = {
                            // "_token": "{{ csrf_token() }}",
                            "id": delete_id,
                        };
                        jQuery.ajax({
                            url: "{!! route('product.prodcut-delete') !!}",
                            type: "POST",
                            data: data,
                            dataType: "json",
                            success: function(res) {

                                if (res.success) {
                                    Swal.fire(
                                        'Deleted!',
                                        'Your file has been deleted.',
                                        'success'
                                    )
                                    $('.dtTable').DataTable().ajax.reload();
                                }
                            }
                        });
                    }
                })
            });
        });

        function applyDtTable(id, url) {
            return $(id).DataTable({
                searching: false,
                processing: true,
                serverSide: true,
                responsive: true,
                // bStateSave: true,
                ajax: {
                    url: url,
                    type: 'POST',
                    data: function(d) {
                        d.search = $('#product_search').val();
                        d.priceMin = $('#min_price').val();
                        d.priceMax = $('#max_price').val();
                        d.qtyMin = $('#min_qty').val();
                        d.qtyMax = $('#max_qty').val();
                        d.status = $("#product_status").val();
                    }
                    // console.log(d.priceMin);
                }
            })
        }
        $('#search_form').on('submit', function(e) {
            e.preventDefault();
            $('.dtTable').DataTable().ajax.reload();
        });

        $("#clear_btn").click(function(e) {
            e.preventDefault();
            $("#product_search").val(''); 
            $("#min_price").val(''); 
            $("#max_price").val(''); 
            $("#min_qty").val(''); 
            $("#max_qty").val(''); 
            $("#product_status").val(''); 
            $('.dtTable').DataTable().ajax.reload();
        });
    </script>
@endpush
