$(document).ready(function() {
    $(".select2").select2({
        placeholder: "Please Select",
    });


});

function display_ct7() {
    var x = new Date()
    var ampm = x.getHours() >= 12 ? ' PM' : ' AM';
    hours = x.getHours() % 12;
    hours = hours ? hours : 12;
    hours = hours.toString().length == 1 ? 0 + hours.toString() : hours;

    var minutes = x.getMinutes().toString()
    minutes = minutes.length == 1 ? 0 + minutes : minutes;

    var seconds = x.getSeconds().toString()
    seconds = seconds.length == 1 ? 0 + seconds : seconds;

    var month = (x.getMonth() + 1).toString();
    month = month.length == 1 ? 0 + month : month;

    var dt = x.getDate().toString();
    dt = dt.length == 1 ? 0 + dt : dt;

    var x1 = month + "/" + dt + "/" + x.getFullYear();
    x1 = x1 + " - " + hours + ":" + minutes + ":" + seconds + " " + ampm;
    document.getElementById('ct7').innerHTML = x1;
    display_c7();
}

function display_c7() {
    var refresh = 1000; // Refresh rate in milli seconds
    mytime = setTimeout('display_ct7()', refresh)
}
display_c7()

var preview = function(event) {
    var output = document.getElementById('fileList');
    output.src = URL.createObjectURL(event.target.files[0]);
    $('.image-preview').addClass('show');
    $('.image-preview').removeClass('hide');
};

function preview_image(event) {
    var total_file = document.getElementById("upload_file").files.length;
    for (var i = 0; i < total_file; i++) {

        $('#image_preview').append("<img style='width: 100px; height: 90px; border: 2px solid #dddddd; margin:5px; padding:10px'  src='" + URL.createObjectURL(event.target.files[i]) + "'>");
        // Set the image's source to a new object URL representing the file, using URL.createObjectURL() to create the blob URL.

    }
};

$(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function() {
    $(".alert-dismissible").alert('close');
});

function printErrorMsg(msg) {
    toastr.error(msg)
}

function printSuccessMsg(msg) {
    toastr.success(msg)
}

function countdowntimes() {
    var livedt = new Date();
    var h = livedt.getHours();
    var m = livedt.getMinutes();
    var s = livedt.getSeconds();
    m = latestTime(m);
    s = latestTime(s);
    document.getElementById('preview').innerHTML =
        h + ":" + m + ":" + s;
    var t = setTimeout(countdowntimes, 500);
}

function latestTime(i) {
    if (i < 10) { i = "0" + i }; // include a zero in front of real clock numbers < 10
    return i;
}