<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $table = 'tbl_product';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $fillable = ['v_name','i_product_code','f_price','f_sale_price','i_qty', 'dt_added_on', 'dt_modified_on', 'i_order', 'ti_status'];

    // public function productImage(
    //     return $this->();
    // )
}
