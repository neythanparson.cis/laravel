@extends('layout.app')
@section('title', $title)
@section('head', $title)

@section('main')
    <section class="content">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">@yield('title')</h3>
            </div>

            <form action="{!! $urlSave !!}" method="POST" enctype="multipart/form-data" id="saveForm" data-parsley-validate>
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label data-placeholder="Choose Categories" for="product">Categories:</label>
                        <select multiple class="select2 form-control" name="categories[]" required data-parsley-required-message="Please select categories">
                            @foreach ($categoryList as $key => $value)
                                <option value="{!! $value->id !!}" {!! in_array($value->id, $productCategories) ? 'selected' : '' !!}> {!! $value->v_name !!}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="product">Product Image: </label>
                        <input name="image[]" type="file" class="form-control" id="upload_file" multiple
                            onchange="preview_image(event)" {!! \Request::route()->getName() == 'product.prodcut-add' ? 'required' : '' !!} data-parsley-required-message="Please choose Image">
                    </div>
                    <div id="image_preview" class="row">
                        @foreach ($image as $key => $value)
                            <div class="img_div col-lg-2 col-sm-12">
                                <img src="{{ $value->v_image ? asset('image/product/' . $value->v_image) : '' }}"style='width: 100px; height: 90px; border: 2px solid #dddddd; margin:5px; padding:10px'>
                                <input type="radio" data-id="{!! $value->id !!}" name="selectedMainImage" value="{!! $value->id !!}" {!! $value->ti_main_image == 1 ? 'checked' : '' !!}>
                                <input type={!! isset($value->ti_main_image) && $value->ti_main_image == '1' ? 'hidden' : 'button' !!} class="btn btn-danger deleteImage" data-id="{!! $value->id !!}" value="Delete">
                            </div>
                        @endforeach
                    </div>
                    <div class="form-group">
                        <label for="product">Product Name: </label>
                        <input name="v_name" type="text" placeholder="Enter Product" class="form-control" value="{!! $product->v_name !!}" autocomplete="off" required data-parsley-required-message="Please enter product name">
                    </div>
                    <div class="form-group">
                        <label for="product">Price:</label>
                        <input name="f_price" type="text" placeholder="Enter Price" class="form-control" value="{!! $product->f_price !!}" autocomplete="off" required data-parsley-required-message="Please enter product price"  data-parsley-type="digits" data-parsley-type-message="Please enter Number Only">
                    </div>
                    <div class="form-group">
                        <label for="product">Sale Price:</label>
                        <input name="f_sale_price" type="text" placeholder="Enter Sale Price" class="form-control" value="{!! $product->f_sale_price !!}" autocomplete="off" required data-parsley-required-message="Please enter product sale-price">
                    </div>
                    <div class="form-group">
                        <label for="product">Quantity:</label>
                        <input name="i_qty" type="text" placeholder="Enter Quantity" class="form-control" value="{!! $product->i_qty !!}" autocomplete="off" required data-parsley-required-message="Please enter product quantity">
                    </div>
                    <div class="form-group">
                        <label for="product">Orders:</label>
                        <input name="i_order" type="text" placeholder="Enter Order" class="form-control" value="{!! $product->i_order !!}" autocomplete="off" required data-parsley-required-message="Please enter no. of orders">
                    </div>
                    <div class="form-group">
                        <label for="product">Status:</label>
                        <select name="ti_status" class="form-control custom-select" required data-parsley-required-message="Please select status">
                            <option value="" selected>Status</option>
                            <option value="1" {{ isset($product->ti_status) && $product->ti_status == 1 ? 'selected' : '' }}>Active</option>
                            <option value="0" {{ isset($product->ti_status) && $product->ti_status == 0 ? 'selected' : '' }}>Deactive</option>
                        </select>
                    </div>
                    <button name="submit" type="submit" class="btn btn-lg btn-primary">
                        <span name="submit">Submit</span>
                    </button>
                </div>
            </form>
        </div>
    </section>
@endsection
@push('script')
    <script>
        jQuery("#saveForm").submit(function(e) {
            e.preventDefault();
            var data = new FormData($('#saveForm')[0]);
            var url = "{!! $urlSave !!}";

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                mimeType: "multipart/form-data",
                dataType: "json",
                contentType: false,
                processData: false,
                // timeout: 5000,
                success: function(response) {
                    console.log(response.url);
                    if ($.isEmptyObject(response.err)) {
                        printSuccessMsg(response.success);
                        window.location.href = response.url;

                    } else {
                        printErrorMsg(response.err);
                    }
                }

            });

        });
    </script>
    <script>
        $(document).on("click", ".deleteImage", function(e) {
            e.preventDefault;
            var el = $(this);
            // console.log(el);
            var delete_id = $(this).data('id');
            var data = {
                "_token": "{{ csrf_token() }}",
                "id": delete_id,
            };
            jQuery.ajax({
                url: "{!! route('product.image-delete') !!}",
                type: "POST",
                data: data,
                dataType: "json",
                success: function(res) {
                    if (res.success) {
                        Swal.fire(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                        )

                        $(el).closest('div').fadeOut(800, function() {
                            $(this).remove();
                        });
                        // $('#image_preview').ajax.reload();
                        // location.reload();
                        // setInterval('location.reload()', 500);
                    }
                }
            });
        });
    </script>
@endpush
